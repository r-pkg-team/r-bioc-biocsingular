Package: BiocSingular
Version: 1.22.0
Date: 2024-09-21
Title: Singular Value Decomposition for Bioconductor Packages
Authors@R: c(person("Aaron", "Lun", role=c("aut", "cre", "cph"),
        email="infinite.monkeys.with.keyboards@gmail.com"))
Imports: BiocGenerics, S4Vectors, Matrix, methods, utils, DelayedArray,
        BiocParallel, ScaledMatrix, irlba, rsvd, Rcpp, beachmat (>=
        2.21.1)
Suggests: testthat, BiocStyle, knitr, rmarkdown, ResidualMatrix
biocViews: Software, DimensionReduction, PrincipalComponent
Description: Implements exact and approximate methods for singular value
        decomposition and principal components analysis, in a framework that
        allows them to be easily switched within Bioconductor packages or 
        workflows. Where possible, parallelization is achieved using 
        the BiocParallel framework.
License: GPL-3
LinkingTo: Rcpp, beachmat, assorthead
VignetteBuilder: knitr
SystemRequirements: C++17
RoxygenNote: 7.2.3
BugReports: https://github.com/LTLA/BiocSingular/issues
URL: https://github.com/LTLA/BiocSingular
git_url: https://git.bioconductor.org/packages/BiocSingular
git_branch: RELEASE_3_20
git_last_commit: aa0f642
git_last_commit_date: 2024-10-29
Repository: Bioconductor 3.20
Date/Publication: 2024-10-29
NeedsCompilation: yes
Packaged: 2024-10-29 23:43:14 UTC; biocbuild
Author: Aaron Lun [aut, cre, cph]
Maintainer: Aaron Lun <infinite.monkeys.with.keyboards@gmail.com>
